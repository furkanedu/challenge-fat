var webpack               = require('webpack');
var path                  = require('path');
var HtmlWebpackPlugin     = require('html-webpack-plugin');

const VENDOR_LIBS = [
  'react', 'redux', 'react-redux', 'react-dom', 'redux-thunk', 'prop-types'
]

module.exports = {
  entry: {
    bundle : './src/index.js',
    vendor : VENDOR_LIBS
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].[chunkhash].js'
  },
  module : {
  	rules : [
  		{
  			use : 'babel-loader',
  			test : /\.js$/,
  			exclude : /node_modules/
  		},
  		{
  			use : [ 'style-loader', 'css-loader' ],
  			test : /\.css$/
  		},
      {
        test: /\.scss$/,
        use: [
          {
            loader: "style-loader" // creates style nodes from JS strings
          },
          {
            loader: "css-loader" // translates CSS into CommonJS
          },
          {
            loader: "sass-loader" // compiles Sass to CSS
          }
        ]
      },
      {
        test:/\.(jpe?g|png|gif|svg)$/,
        use: [
          {
            loader : 'url-loader',
            options : { limit : 40000 }
          },
          'image-webpack-loader'
        ]
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader?name=public/fonts/[name].[ext]'
      }
  	]
  },
  devServer: {
    historyApiFallback: true,
    inline:true,
    port: 5001
  }, 
  plugins : [
    new webpack.optimize.CommonsChunkPlugin({
      names : [ 'vendor', 'manifest' ]
    }),
    new HtmlWebpackPlugin({
      template : 'src/index.html'
    })
  ]
};
