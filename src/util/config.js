const env = process.env.NODE_ENV;

export const config = {

  env,

  // Local server
  // host : process.env.host || (location.href.includes('edemo') || location.href.includes('localhost')? decodeURIComponent(atob('aHR0cDovL2xvY2FsaG9zdDo4MDgx').split('').map(function(c) { return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2); }).join('')): ''),

  // Production
  host : 'http://localhost:8080',

  mockApi : env === 'demo' || localStorage.getItem('mock') === 'true',

  loggerActive : true

};

export default config;





