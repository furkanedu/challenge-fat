import { browserHistory }                    from 'react-router';
import { appStore }                          from '../index'

export default class Interceptors {

  setHeaders(request){

      this._setAccessTokenHeader(request);

      this._setDefaultHeaders(request);

  }

  interceptResponse( request, resolve, reject ){

      request.end( ( error, response ) => {

          if( error ) this._handleError( response, reject );
          
          response = response ? response.body : {}
          
          if( response && response.internal_version ){
            if( !window.internal_version ){
              window.internal_version = response.internal_version
            } else {
              if( window.internal_version !== response.internal_version ){
                window.location.reload()
              }
            }
          }
          
          return resolve( response );

      } )

  }

  _handleError( response, reject ){

      if( !response ) return reject({ error : 'There is an error.' });

      if( response.status === 401 ){
          
          if( window.location.href.indexOf('login') === -1 ){
            // clearAuthData()
            browserHistory.push("/login")
          }

      }

      if( response.status === 409 ){
        return reject({
           ...response.body,
           anotherUserLoggedIn : true
        });  
      }

      return reject( response.body );

  }

  _setAccessTokenHeader(request){

      // let authData = getAuthData();

      // if( authData &&  authData.api_token ){

      //     request.set('X-Access-Token', authData.api_token );

      // }

  }

  _setDefaultHeaders(request){

      request.set('Accept', 'application/json');

      request.set('Content-Type', 'application/json');

  }

}
