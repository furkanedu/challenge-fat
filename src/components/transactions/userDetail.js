import React, { Component }   from 'react'
import PropTypes              from 'prop-types'
import classNames             from 'classnames'
import { connect }            from 'react-redux'
import { bindActionCreators } from 'redux'
import { Link }               from 'react-router'

import * as transactionsActions from './redux'

import NavBar                 from '../_shared/layout/navbar'

@connect(
  state => ({
    transactionsState : state.transactions
  }),
  dispatch => ({
 		 transactionsActions : bindActionCreators( transactionsActions, dispatch )
  })
)
export default class UserDetail extends Component {
  
  static propTypes = {
    name: PropTypes.string
  }

  componentDidMount(){
    let {
      params = {}
    } = this.props

    let {
      user_id = ''
    } = params

    if( !user_id ){
      console.error( 'user_id is not found on route params.' )
    }
  	
    this.props.transactionsActions.getUserDetail( user_id )
  }

  componentWillUnmount(){
    this.resetReduxState()
  }

  resetReduxState(){
    this.props.transactionsActions.setRootReduxStateProp_multiple({
      userDetail : {},
      loadingUserDetail : false,
      loadedUserDetail : false,
      userDetail_error : false
    })
  }

  goBack = event => {
    event.preventDefault()
    if( this.props.router && this.props.router.goBack ){
      this.props.router.goBack()
    }
  }

  render() {

    let {
      params = {},
      transactionsState
    } = this.props

    let {
      user_id = ''
    } = params

    if( !user_id ) return <div>Upps...</div>

    let {
      userDetail = {},
      loadedUserDetail,
      userDetail_error
    } = transactionsState

    let {
      firstName,
      lastName,
      email,
      credit,
      profileImgUrl,
      telephone,
    } = userDetail

    return (
      <div>
        <NavBar />
        
        <div className="mb-4 ml-12 mt-10">
          <span className="font-bold font-sans text-grey-dark text-lg text-xl">
            <Link to="/transactions" className="font-bold font-sans text-grey-dark text-lg text-xl no-underline text-grey-dark">
              Transactions
            </Link>
          </span>

          &nbsp;

          <span className="font-bold font-sans text-lg text-xl"> > </span>

          &nbsp;

          <span className="font-bold font-sans text-grey-dark text-lg text-xl">
            <a href="" onClick={ this.goBack } className="font-bold font-sans text-grey-dark text-lg text-xl no-underline text-grey-dark">
              Transaction Detail
            </a>
          </span>


          &nbsp;

          <span className="font-bold font-sans text-lg text-xl"> > </span>

          &nbsp;

          <span className="font-bold font-sans text-lg text-xl">
            User Detail
          </span>
        </div>

        {
          userDetail_error && <img src="https://www.hera.org.nz/wp-content/uploads/20150918_Notice_HERAreportR4-103Error_STRUC.jpg" alt="" style={{ width: '40%' ,margin: 'auto', display: 'block' }}/>
        }
          

        {
          !userDetail_error &&
          <div className="bg-white m-12 mt-0 p-8" >
            <div className="flex justify-between mb-6">
              <div className="flex text-lg">
                <div className="mr-6 text-grey-dark font-mono">
                  USER ID:
                </div>
                <div className="font-bold font-mono"> 
                  { user_id }
                </div>
              </div>
            </div>

            {
              !loadedUserDetail && 
              <div className="flex" style={{ height:'200px' }}>

              </div>
            }

            {
              loadedUserDetail &&
              <div className="flex">

                <div className="flex-1">
                  
                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">FIRST NAME:</div>
                    <div className="font-bold font-mono">{ firstName }</div>
                  </div>

                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">LAST NAME:</div>
                    <div className="font-bold font-mono">{ lastName }</div>
                  </div>

                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">EMAIL:</div>
                    <div className="font-bold font-mono">{ email }</div>
                  </div>

                </div>


                <div className="flex-1">

                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">TELEPHONE:</div>
                    <div className="font-bold font-mono">{ telephone }</div>
                  </div>

                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">CREDIT:</div>
                    <div className="font-bold font-mono">{ credit }</div>
                  </div>

                </div>
                
                <div className="flex-1">

                </div>
              </div>
            }
              
          </div>
        }

  
      </div>
    )
  }
}