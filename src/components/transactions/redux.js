import Fetcher                          from '../../util/Request'
// import { defineAction }                 from 'redux-define'
// import {
//   showToaster,
//   showSpinner,
//   hideSpinner
// }                                        from '../_Helpers/actions'

const
  namespace = 'Transactions@',
  // subActions = ['SUCCESS', 'ERROR'],

  /************ CONSTANTS ************/

  /* for async actions , includes subactions */

  /* for normal actions , doesn t include subactions */
  SET_ROOT_REDUX_STATE  = `${namespace}SET_ROOT_REDUX_STATE__`,
  INITIALIZE_REDUX_STATE  = `${namespace}INITIALIZE_REDUX_STATE__`,

  /* initial state for this part of the redux state */
  initialState = {
    transactions : [],
    transactionDetail : {},
    loadingTransactionDetail : false,
    loadedTransactionDetail : false,
    transactionDetail_error : false,
    userDetail : {},
    loadingUserDetail : false,
    loadedUserDetail : false,
    userDetail_error : false
  }

/* ---- ---- ---- ---- ---- ---- ---- ---- ---- REDUCER ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- */

export default function reducer(state = initialState, action) {

  switch(action.type) {

    case SET_ROOT_REDUX_STATE:
      return {
        ...state,
        ...action.data
      }
    case INITIALIZE_REDUX_STATE:
      return {
        ...initialState,
        //badgeCounterValues : state.badgeCounterValues
      }
    default:
      return state

  }

}


/* ---- ---- ---- ---- ---- ---- ---- ---- ---- ACTIONS ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- */

/*
  this is a common purpose action creator which updates this part
  of the redux state tree, rather than creating different action
  creators, this one might be used to change a single property
  of this part of the redux tree
*/
export function setRootReduxStateProp( field, value ){
  return function( dispatch, getState ){
    dispatch({
      type: SET_ROOT_REDUX_STATE,
      data : { [ field ] : value }
    })
    return Promise.resolve()
  }
}

export function setRootReduxStateProp_multiple( keysToUpdate = {} ){
  return function( dispatch, getState ){
    dispatch({
      type: SET_ROOT_REDUX_STATE,
      data : { ...keysToUpdate }
    })
    return Promise.resolve()
  }
}

export function initializeGridReduxState(){
  return function( dispatch, getState ){
    dispatch({ type: INITIALIZE_REDUX_STATE })
    return Promise.resolve()
  }
}

/* ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- */



/*==============================================
=                   SECTION                    =
==============================================*/


export function getTransactions(){
  return function( dispatch, getState ) {

    const fetcher = new Fetcher()

    return fetcher
      .fetch(`/transactions/0`, {
        method : 'get',
        data : {}
      }).then( 
        response => {
          console.log('response is for getTransactions : ', response)
          dispatch({ type : SET_ROOT_REDUX_STATE, data : { transactions : response  } })
      }).catch( error => {
        console.log('error is for getTransactions: ' , error)
      })
  }
}

export function getTransactionDetail( id = '' ){
  return function( dispatch, getState ) {

    const fetcher = new Fetcher()

    dispatch({
      type : SET_ROOT_REDUX_STATE,
      data : {
        loadingTransactionDetail : true,
        loadedTransactionDetail : false,
        transactionDetail_error : false
      }
    })

    return fetcher
      .fetch(`/transaction/${id}`, {
        method : 'get',
        data : {}
      }).then( 
        response => {
          console.log('response is for getTransaction : ', response)
          dispatch({
            type : SET_ROOT_REDUX_STATE,
            data : {
              loadingTransactionDetail : false,
              loadedTransactionDetail : true,
              transactionDetail : response  
            }
          })
      }).catch( error => {
        console.log('error is for getTransactions: ' , error)
        dispatch({
          type : SET_ROOT_REDUX_STATE,
          data : {
            loadingTransactionDetail : false,
            transactionDetail : {},
            transactionDetail_error : true
          }
        })
      })
  }
}

export function getUserDetail( id = '' ){
  return function( dispatch, getState ) {

    const fetcher = new Fetcher()

    dispatch({
      type : SET_ROOT_REDUX_STATE,
      data : {
        loadingUserDetail : true,
        loadedUserDetail : false,
        userDetail_error : false
      }
    })

    return fetcher
      .fetch(`/user/${id}`, {
        method : 'get',
        data : {}
      }).then( 
        response => {
          console.log('response is for getUserDetail : ', response)
          dispatch({
            type : SET_ROOT_REDUX_STATE,
            data : {
              loadingUserDetail : false,
              loadedUserDetail : true,
              userDetail : response  
            }
          })
      }).catch( error => {
        console.log('error is for getUserDetails: ' , error)
        dispatch({
          type : SET_ROOT_REDUX_STATE,
          data : {
            loadingUserDetail : false,
            userDetail : {},
            userDetail_error : true
          }
        })
      })
  }
}

export function approveTransaction( id = '' ){
  return function( dispatch, getState ) {

    const fetcher = new Fetcher()

    dispatch({
      type : SET_ROOT_REDUX_STATE,
      data : {
        loadingTransactionDetail : true,
        loadedTransactionDetail : false,
        transactionDetail_error : false
      }
    })

    return fetcher
      .fetch(`/transaction/${id}`, {
        method : 'put',
        data : { status : 'FL_APPROVED' }
      }).then( 
        response => {
          getTransactionDetail( id )( dispatch, getState )
          console.log('response is for approveTransaction : ', response)
          dispatch({
            type : SET_ROOT_REDUX_STATE,
            data : {
              loadingTransactionDetail : false,
              loadedTransactionDetail : true,
              transactionDetail : response  
            }
          })
      }).catch( error => {
        console.log('error is for approveTransactions: ' , error)
        dispatch({
          type : SET_ROOT_REDUX_STATE,
          data : {
            loadingTransactionDetail : false,
            transactionDetail : {},
            transactionDetail_error : true
          }
        })
      })
  }
}

  
/*=====  End of FILTER RELATED ACTIONS  ======*/
			
    