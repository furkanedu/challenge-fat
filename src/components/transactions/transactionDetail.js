import React, { Component }   from 'react'
import PropTypes              from 'prop-types'
import classNames             from 'classnames'
import { connect }            from 'react-redux'
import { bindActionCreators } from 'redux'
import { Link }               from 'react-router'

import * as transactionsActions from './redux'

import NavBar                 from '../_shared/layout/navbar'

@connect(
  state => ({
    transactionsState : state.transactions
  }),
  dispatch => ({
 		 transactionsActions : bindActionCreators( transactionsActions, dispatch )
  })
)
export default class TransactionDetail extends Component {
  
  static propTypes = {
    name: PropTypes.string
  }

  componentDidMount(){
    let {
      params = {}
    } = this.props

    let {
      transaction_id = ''
    } = params

    if( !transaction_id ){
      console.error( 'transaction_id is not found on route params.' )
    }
  	
    this.props.transactionsActions.getTransactionDetail( transaction_id )
  }

  componentWillUnmount(){
    this.resetReduxState()
  }

  resetReduxState(){
    this.props.transactionsActions.setRootReduxStateProp_multiple({
      transactionDetail : {},
      loadingTransactionDetail : false,
      loadedTransactionDetail : false,
      transactionDetail_error : false
    })
  }

  approveTransaction = event => {
    let {
      params = {},
      transactionsActions
    } = this.props

    let {
      transaction_id
    } = params

    transactionsActions.approveTransaction( transaction_id )
  }

  render() {

    let {
      params = {},
      transactionsState
    } = this.props

    let {
      transaction_id = ''
    } = params

    if( !transaction_id ) return <div>Upps...</div>

    let {
      transactionDetail = {},
      loadedTransactionDetail,
      transactionDetail_error
    } = transactionsState

    let {
      borrowerId,
      creditUsed,
      currency,
      fromDate,
      id,
      itemId,
      lenderId,
      price,
      promocode,
      status,
      toDate,
      totalDiscount,
    } = transactionDetail

    return (
      <div>
        <NavBar />
        
        <div className="mb-4 ml-12 mt-10">
          <span className="font-bold font-sans text-grey-dark text-lg text-xl">
            <Link to="/transactions" className="font-bold font-sans text-grey-dark text-lg text-xl no-underline text-grey-dark">
              Transactions
            </Link>
          </span>

          &nbsp;

          <span className="font-bold font-sans text-lg text-xl"> > </span>

          &nbsp;

          <span className="font-bold font-sans text-lg text-xl">
            Transaction Detail
          </span>
        </div>

        {
          transactionDetail_error && <img src="https://www.hera.org.nz/wp-content/uploads/20150918_Notice_HERAreportR4-103Error_STRUC.jpg" alt="" style={{ width: '40%' ,margin: 'auto', display: 'block' }}/>
        }
          

        {
          !transactionDetail_error &&
          <div className="bg-white m-12 mt-0 p-8" >
            <div className="flex justify-between mb-6">
              <div className="flex text-lg">
                <div className="mr-6 text-grey-dark font-mono">
                  TRANSACTION ID:
                </div>
                <div className="font-bold font-mono"> 
                  { id }
                </div>
              </div>
              {
                loadedTransactionDetail && status !== 'FL_APPROVED' &&
                <button 
                  className="bg-green-light cursor-pointer flex font-sans hover:bg-green-lighter items-center justify-center px-6 py-2 rounded text-base text-white font-mono"
                  onClick={ this.approveTransaction }
                >
                  APPROVE TRANSACTION
                </button>
              }
                
            </div>

            {
              !loadedTransactionDetail && 
              <div className="flex" style={{ height:'200px' }}>

              </div>
            }

            {
              loadedTransactionDetail &&
              <div className="flex">

                <div className="flex-1">
                  
                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">STATUS:</div>
                    <div>
                      {
                        status === 'FL_APPROVED' ?
                        <div className="-mb-10 border border-green border-solid font-bold font-mono p-1 text-green relative" style={{ bottom: '3px' }}>
                          { status.replace(/_/g, ' ' ) }
                        </div> :
                        <div className="font-bold font-mono">
                          { status.replace(/_/g, ' ' ) }
                        </div>
                      }
                        
                    </div>
                  </div>

                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">ITEM ID:</div>
                    <div className="font-bold font-mono">{ itemId }</div>
                  </div>

                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">LENDER ID:</div>
                    <div>
                      <Link to={`user/${lenderId}`} className="font-bold font-mono text-black">
                        { lenderId }
                      </Link>
                    </div>
                  </div>

                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">BORROWER ID:</div>
                    <div>
                      <Link to={`user/${borrowerId}`} className="font-bold font-mono text-black">
                        { borrowerId }
                      </Link>
                    </div>
                  </div>

                </div>


                <div className="flex-1">

                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">PRICE:</div>
                    <div className="font-bold font-mono">{ price }</div>
                  </div>

                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">CURRENCY:</div>
                    <div className="font-bold font-mono">{ currency }</div>
                  </div>

                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">CRED USED:</div>
                    <div className="font-bold font-mono">{ creditUsed }</div>
                  </div>

                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">TOTAL DISCOUNT:</div>
                    <div className="font-bold font-mono">{ totalDiscount }</div>
                  </div>

                </div>
                
                <div className="flex-1">

                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">PROMO CODE:</div>
                    <div className="font-bold font-mono">{ promocode }</div>
                  </div>

                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">FROM DATE:</div>
                    <div className="font-bold font-mono">{ fromDate.slice( 0, 10 ) }</div>
                  </div>

                  <div className="flex justify-between pr-16 py-4">
                    <div className="font-bold font-mono text-grey">TO DATE:</div>
                    <div className="font-bold font-mono">{ toDate.slice( 0, 10 ) }</div>
                  </div>
                </div>
              </div>
            }
              
          </div>
        }

  
      </div>
    )
  }
}