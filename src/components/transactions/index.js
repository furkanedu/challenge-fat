import React, { Component }   from 'react'
import PropTypes              from 'prop-types'
import classNames             from 'classnames'
import { connect }            from 'react-redux'
import { bindActionCreators } from 'redux'
import { Link }               from 'react-router'

import * as transactionsActions from './redux'

import NavBar                 from '../_shared/layout/navbar'

@connect(
  state => ({
    transactionsState : state.transactions
  }),
  dispatch => ({
 		 transactionsActions : bindActionCreators( transactionsActions, dispatch )
  })
)
export default class Transactions extends Component {
  
  static propTypes = {
    name: PropTypes.string
  }

  componentDidMount(){
  	this.props.transactionsActions.getTransactions()
  }

  componentWillUnmount(){
    this.resetReduxState()
  }

  resetReduxState(){
    this.props.transactionsActions.setRootReduxStateProp_multiple({
      transactions : []
    })
  }

  render() {

    let {
      transactions = []
    } = this.props.transactionsState

    return (
      <div>
        <NavBar />
        
        <div className="mb-4 ml-12 mt-10">
          <span className="font-bold font-sans text-lg text-xl">
            <span className="font-bold font-sans text-lg text-xl no-underline">
              Transactions
            </span>
          </span>
        </div>
          

        <div className="bg-white m-12 mt-0">

          <table className="w-full" style={{ tableLayout: 'fixed', borderCollapse: 'collapse' }}>
            <colgroup>
              <col style={{width: "200px"}}/>
              <col style={{width: "200px"}}/>
              <col style={{width: "100px"}}/>
              <col style={{width: "100px"}}/>
              <col style={{width: "100px"}}/>
              <col style={{width: "100px"}}/>
              <col style={{width: "150px"}}/>
              <col style={{width: "150px"}}/>
              <col style={{width: "150px"}}/>
            </colgroup>
            <thead className="border-b border-grey-light border-solid font-sans text-grey" style={{ lineHeight: '3' }}>
              <tr>
                <th className="text-left">
                  <span className="pl-8">ID</span>
                </th>
                <th className="text-left">
                  <span>STATUS</span>
                </th>
                <th className="text-left">
                  <span>PRICE</span>
                </th>
                <th className="text-left">
                  <span>CURRENCY</span>
                </th>
                <th className="text-left">
                  <span>CRED. USED</span>
                </th>
                <th className="text-left">
                  <span>TOTAL DIS.</span>
                </th>
                <th className="text-left">
                  <span>PROMO CODE</span>
                </th>
                <th className="text-left">
                  <span>FROM DATE</span>
                </th>
                <th className="text-left">
                  <span>TO DATE</span>
                </th>
              </tr>
            </thead>
            <tbody>
              {
                transactions.map(
                  ({  
                    creditUsed,
                    currency,
                    fromDate,
                    id,
                    price,
                    promocode,
                    status,
                    toDate,
                    totalDiscount,
                  }) => <tr className="font-mono text-grey-darker" style={{ lineHeight: '4' }} key={ `transactions-record-${id}` }>
                    <td className="truncate pl-8">
                      <Link to={`transaction/${id}`} className="text-grey-darker">
                        { id }
                      </Link>
                    </td>
                    <td>
                      {
                        status === 'FL_APPROVED' ?
                        <div className="font-bold font-mono text-green" style={{ bottom: '3px' }}>
                          { status.replace(/_/g, ' ' ) }
                        </div> :
                        <div className="font-bold font-mono">
                          { status.replace(/_/g, ' ' ) }
                        </div>
                      }
                    </td>
                    <td className="truncate">{ price }</td>
                    <td className="truncate">{ currency }</td>
                    <td className="truncate">{ creditUsed }</td>
                    <td className="truncate">{ totalDiscount }</td>
                    <td className="truncate">{ promocode }</td>
                    <td className="truncate">{ fromDate.slice(0, 10) }</td>
                    <td className="truncate">{ toDate.slice(0, 10) }</td>
                  </tr>
                )
              }
              
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}