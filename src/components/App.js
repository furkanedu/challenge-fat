import React, { Component }       from 'react'
import PropTypes                  from 'prop-types'

import '../../style/components/transactions/main.scss'

export default class AppContainer extends Component {
  
  static propTypes = {
    name: PropTypes.string
  }

  render() {

    return (

      <div>
      	{ this.props.children }
      </div>
    )
  }
}