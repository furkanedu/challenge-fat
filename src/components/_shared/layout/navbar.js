import React from 'react'

const NavBar = () => <div className="h-16 shadow flex bg-white">
	<div className="h-full homepage-logo-container ml-10 p-2">
		<img className="h-full" src="https://assets.fatlama.com/static/img/rebrand-assets/logo/logo-llama-text-01@2x.png" alt="fat llama logo text"/>
		<img className="h-full" src="https://assets.fatlama.com/static/img/rebrand-assets/logo/logo-llama-01@2x.png" alt="fat llama logo image"/>
	</div>
	<div className="font-bold font-sans self-center text-2xl" style={{ position: 'absolute', left: 'calc( 50% - 145px )', color: '#6CC7BE' }}>
		FRONTEND CHALLENGE
	</div>
</div> 

export default NavBar