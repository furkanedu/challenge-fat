import    React                         from 'react'
import    ReactDOM                      from 'react-dom'

import '../style/shared/index.scss'

// redux
import {
  combineReducers,
  createStore,
  applyMiddleware,
  compose }                             from 'redux'

import {browserHistory }                from 'react-router'
import {
  syncHistoryWithStore,
  routerReducer 
}                       								from 'react-router-redux'
import  thunk                           from 'redux-thunk'

// main reducers
import transactionsReducer              from './components/transactions/redux'

import Routes                           from './routes'

const appReducer = combineReducers({
	transactions 			: transactionsReducer,
	routing 		: routerReducer
})

const rootReducer = (state, action) => {
  if ( [ 'LOGOUT' ].includes( action.type ) ) {
    const { routing } = state
    state = { routing } 
  }
  return appReducer(state, action)
}

const appStore = createStore(
  rootReducer,
  compose(
    applyMiddleware(thunk),
    window.devToolsExtension && process.env.NODE_ENV !== 'production' ? window.devToolsExtension() : f => f
  )
)

const appHistory = syncHistoryWithStore( browserHistory, appStore )

require('es6-object-assign').polyfill()

const renderApplication = () => {
  if( true ){ // change the condition if you need to wait for a global variable to be true before running SPA
    ReactDOM.render(
      <Routes 
      	store={appStore} 
      	appHistory={appHistory}  
      /> , 
      document.getElementById('root')
    )
  }else{
    setTimeout( 
      () => {
        renderApplication()
      },
      500
    )
  }
}

renderApplication()