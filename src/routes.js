import React          from 'react'
import { 
  Router, 
  Route, 
  IndexRoute,
  Link
}                     from 'react-router'
import  { Provider }  from 'react-redux'

import AppContainer   from './components/App'

const IndexRouteTEMP = () => <div>
  <Link to="/transactions">Transactions</Link>
</div>

const ComponentRoutes = {
  component     : AppContainer,
  path          : '/',
  indexRoute: { onEnter: (nextState, replace) => replace('/transactions') },
  childRoutes   : [
    {
      path : '/transactions',
      getComponent( location, cb ){
        System.import( './components/transactions' ).then(
          module => cb( null, module.default )
        )
      }
    },
    {
      path : '/transaction/:transaction_id',
      getComponent( location, cb ){
        System.import( './components/transactions/transactionDetail' ).then(
          module => cb( null, module.default )
        )
      }
    },
    {
      path : '/user/:user_id',
      getComponent( location, cb ){
        System.import( './components/transactions/userDetail' ).then(
          module => cb( null, module.default )
        )
      }
    }
  ]
}

const Routes = ({ store, appHistory }) => {
  return (
    <Provider store={store}>
      <Router 
        history={ appHistory }
        routes={ ComponentRoutes }
      />
    </Provider>
  )
}

export default Routes
