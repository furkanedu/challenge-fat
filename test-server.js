// test build by serving it via :5000

const express = require('express');
const path = require('path');
const app = express();
app.set('port', (process.env.PORT || 5000))
app.use(express.static('./dist'))

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, './dist', 'index.html'));
})

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});